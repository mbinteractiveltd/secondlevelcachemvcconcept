namespace AdamMvcConceptTest.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("BlogPost")]
    public partial class BlogPost
    {
        public BlogPost()
        {
            BlogPostMetaInformations = new HashSet<BlogPostMetaInformation>();
            BlogPostTagLinks = new HashSet<BlogPostTagLink>();
        }

        public int ID { get; set; }

        [Required]
        public string Title { get; set; }

        [Column(TypeName = "ntext")]
        [Required]
        public string Content { get; set; }

        public DateTime Created { get; set; }

        [Required]
        public string Guid { get; set; }

        public DateTime Modified { get; set; }

        public int StatusId { get; set; }

        public bool? Sticky { get; set; }

        public string SomeOtherField { get; set; }

        public virtual BlogPostStatu BlogPostStatu { get; set; }

        public virtual ICollection<BlogPostMetaInformation> BlogPostMetaInformations { get; set; }

        public virtual ICollection<BlogPostTagLink> BlogPostTagLinks { get; set; }
    }
}
