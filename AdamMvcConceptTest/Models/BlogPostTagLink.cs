namespace AdamMvcConceptTest.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("BlogPostTagLink")]
    public partial class BlogPostTagLink
    {
        public int ID { get; set; }

        public int PostId { get; set; }

        public int TagId { get; set; }

        public virtual BlogPost BlogPost { get; set; }

        public virtual BlogPostTag BlogPostTag { get; set; }
    }
}
