namespace AdamMvcConceptTest.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class BlogPostStatu
    {
        public BlogPostStatu()
        {
            BlogPosts = new HashSet<BlogPost>();
        }

        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        public string StatusName { get; set; }

        public virtual ICollection<BlogPost> BlogPosts { get; set; }
    }
}
