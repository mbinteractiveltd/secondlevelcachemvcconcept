namespace AdamMvcConceptTest.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class adamsDbContext : DbContext
    {
        public adamsDbContext()
            : base("name=adamsDbContext")
        {
        }

        public virtual DbSet<BlogPost> BlogPosts { get; set; }
        public virtual DbSet<BlogPostMetaInformation> BlogPostMetaInformations { get; set; }
        public virtual DbSet<BlogPostStatu> BlogPostStatus { get; set; }
        public virtual DbSet<BlogPostTag> BlogPostTags { get; set; }
        public virtual DbSet<BlogPostTagLink> BlogPostTagLinks { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BlogPost>()
                .HasMany(e => e.BlogPostMetaInformations)
                .WithRequired(e => e.BlogPost)
                .HasForeignKey(e => e.PostId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BlogPost>()
                .HasMany(e => e.BlogPostTagLinks)
                .WithRequired(e => e.BlogPost)
                .HasForeignKey(e => e.PostId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BlogPostStatu>()
                .HasMany(e => e.BlogPosts)
                .WithRequired(e => e.BlogPostStatu)
                .HasForeignKey(e => e.StatusId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BlogPostTag>()
                .Property(e => e.TagDescription)
                .IsUnicode(false);

            modelBuilder.Entity<BlogPostTag>()
                .HasMany(e => e.BlogPostTagLinks)
                .WithRequired(e => e.BlogPostTag)
                .HasForeignKey(e => e.TagId)
                .WillCascadeOnDelete(false);
        }
    }
}
