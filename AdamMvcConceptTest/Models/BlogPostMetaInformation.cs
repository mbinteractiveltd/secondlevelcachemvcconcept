namespace AdamMvcConceptTest.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("BlogPostMetaInformation")]
    public partial class BlogPostMetaInformation
    {
        public int ID { get; set; }

        public int PostId { get; set; }

        [Required]
        public string Key { get; set; }

        [Required]
        public string Value { get; set; }

        public virtual BlogPost BlogPost { get; set; }
    }
}
