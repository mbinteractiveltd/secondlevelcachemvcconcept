namespace AdamMvcConceptTest.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("BlogPostTag")]
    public partial class BlogPostTag
    {
        public BlogPostTag()
        {
            BlogPostTagLinks = new HashSet<BlogPostTagLink>();
        }

        public int ID { get; set; }

        [Required]
        public string TagName { get; set; }

        [Column(TypeName = "text")]
        public string TagDescription { get; set; }

        public virtual ICollection<BlogPostTagLink> BlogPostTagLinks { get; set; }
    }
}
