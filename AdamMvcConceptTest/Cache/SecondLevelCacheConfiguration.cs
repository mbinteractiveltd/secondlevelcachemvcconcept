﻿using EFCache;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Common;
using System.Linq;
using System.Web;

namespace AdamMvcConceptTest.Cache
{
    public class SecondLevelCacheConfiguration : DbConfiguration
    {
        public SecondLevelCacheConfiguration()
          {
            var transactionHandler = new CacheTransactionHandler(new InMemoryCache());
 
            AddInterceptor(transactionHandler);
 
            CachingPolicy cachePol = new CachingPolicy();
            
           

            Loaded +=
              (sender, args) => args.ReplaceService<DbProviderServices>(
                (s, _) => new CachingProviderServices(s, transactionHandler));
          }
    }
}