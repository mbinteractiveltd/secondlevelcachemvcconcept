﻿using EFCache;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdamMvcConceptTest.Cache
{
    /// <summary>
    /// Overrides EFCache abstract virtual memmebrs
    /// 
    /// Need to provide a proper caching implementation here, particularly for the GetExpirationTimeout and CanBeCached methods which we will be able to use to great effect if done right
    /// </summary>
    public class BrandHubCachingPolicy : CachingPolicy
    {
        protected override bool CanBeCached(System.Collections.ObjectModel.ReadOnlyCollection<System.Data.Entity.Core.Metadata.Edm.EntitySetBase> affectedEntitySets, string sql, IEnumerable<KeyValuePair<string, object>> parameters)
        {
            return base.CanBeCached(affectedEntitySets, sql, parameters);
        }
        protected override void GetCacheableRows(System.Collections.ObjectModel.ReadOnlyCollection<System.Data.Entity.Core.Metadata.Edm.EntitySetBase> affectedEntitySets, out int minCacheableRows, out int maxCacheableRows)
        {
            base.GetCacheableRows(affectedEntitySets, out minCacheableRows, out maxCacheableRows);
        }
        protected override void GetExpirationTimeout(System.Collections.ObjectModel.ReadOnlyCollection<System.Data.Entity.Core.Metadata.Edm.EntitySetBase> affectedEntitySets, out TimeSpan slidingExpiration, out DateTimeOffset absoluteExpiration)
        {
            base.GetExpirationTimeout(affectedEntitySets, out slidingExpiration, out absoluteExpiration);
        }
    }
}