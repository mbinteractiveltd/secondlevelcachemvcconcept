namespace AdamMvcConceptTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FirstMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BlogPostMetaInformation",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        PostId = c.Int(nullable: false),
                        Key = c.String(nullable: false),
                        Value = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.BlogPost", t => t.PostId)
                .Index(t => t.PostId);
            
            CreateTable(
                "dbo.BlogPost",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false),
                        Content = c.String(nullable: false, storeType: "ntext"),
                        Created = c.DateTime(nullable: false),
                        Guid = c.String(nullable: false),
                        Modified = c.DateTime(nullable: false),
                        StatusId = c.Int(nullable: false),
                        Sticky = c.Boolean(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.BlogPostStatus", t => t.StatusId)
                .Index(t => t.StatusId);
            
            CreateTable(
                "dbo.BlogPostStatus",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        StatusName = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.BlogPostTagLink",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        PostId = c.Int(nullable: false),
                        TagId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.BlogPostTag", t => t.TagId)
                .ForeignKey("dbo.BlogPost", t => t.PostId)
                .Index(t => t.PostId)
                .Index(t => t.TagId);
            
            CreateTable(
                "dbo.BlogPostTag",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TagName = c.String(nullable: false),
                        TagDescription = c.String(unicode: false, storeType: "text"),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BlogPostTagLink", "PostId", "dbo.BlogPost");
            DropForeignKey("dbo.BlogPostTagLink", "TagId", "dbo.BlogPostTag");
            DropForeignKey("dbo.BlogPost", "StatusId", "dbo.BlogPostStatus");
            DropForeignKey("dbo.BlogPostMetaInformation", "PostId", "dbo.BlogPost");
            DropIndex("dbo.BlogPostTagLink", new[] { "TagId" });
            DropIndex("dbo.BlogPostTagLink", new[] { "PostId" });
            DropIndex("dbo.BlogPost", new[] { "StatusId" });
            DropIndex("dbo.BlogPostMetaInformation", new[] { "PostId" });
            DropTable("dbo.BlogPostTag");
            DropTable("dbo.BlogPostTagLink");
            DropTable("dbo.BlogPostStatus");
            DropTable("dbo.BlogPost");
            DropTable("dbo.BlogPostMetaInformation");
        }
    }
}
