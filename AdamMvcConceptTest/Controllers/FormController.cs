﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AdamMvcConceptTest.Models;

namespace AdamMvcConceptTest.Controllers
{
    public class FormController : Controller
    {
        private adamsDbContext context;

        public FormController()
        {
            context = new adamsDbContext();         
        }
        // GET: Form
        public ActionResult Index()
        {
            BlogPost bah;

            bah = context.BlogPosts.FirstOrDefault();
        

            return View(bah);
        }
        [HttpPost]
        public ActionResult Index(BlogPost post)
        {
            if (ModelState.IsValid)
            {

                context.Entry(post).State = EntityState.Modified;

                try
                {
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    HttpContext.Response.Write(ex.ToString());
                }

                return View(post);
            }
            else
            {
                return View(post);
            }
        }
    }
}