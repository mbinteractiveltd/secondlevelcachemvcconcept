﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AdamMvcConceptTest.Models;

namespace AdamMvcConceptTest.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index() {
            List<BlogPost> posts = new List<BlogPost>();

            using (var db = new adamsDbContext())
            {
                posts = db.BlogPosts.Where(i => i.Title != "").ToList();
            }
            
            return View(posts);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Update(List<BlogPost> posts)
        {
            Console.WriteLine(posts);
            return View("Index", posts);
        }
    }
}